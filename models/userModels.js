const mongoose = require('mongoose')
const validator=require('validator')

const userSchema= new mongoose.Schema({
    name:{
        type: String,
        required: [true, 'Please tell ys your name!'],
    },
    email:{
        type: String,
        required: [true, 'please provide your email'],
        unique: true,
        lowercase: true,
        validate: [validator.isEmail, 'please provide a valid email'],
    },
    photo:{
        type: String,
        default: 'default.jpg'
    },
    role: {
        type:String,
        enum: ['user','sme','pharmacist', 'admin'],
        default: 'user',
    },
    password:{
        type: String,
        required: [true, 'Please provide a password!'],
        minleangth:8,
        // password wont be included when we get the users
        select: false,
    },
    active:{
        type: Boolean,
        default: true,
        select: false,
    },
})